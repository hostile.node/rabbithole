//------------------------------------------------------------------------------
// This file is part of Rabbit Hole.
//
// Rabbit Hole is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rabbit Hole is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rabbit Hole. If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#pragma once

#include "iimport.h"

namespace RabbitHole {
namespace Import {

class Nmap : public IImport
{
	// From IImport.
	virtual std::string GetName() const override;
	virtual bool IsSupported(IImport::Context context) const override;
	virtual const std::vector<std::string> GetSupportedNodes() const override;
	virtual const std::vector<std::string> GetExtensions() const override;
	virtual ImportResult* GlobalImport(const std::string& filename) override;
	virtual ImportResult* NodeImport(const std::string& filename, Node* pStartingNode) override;
};

} // namespace Import
} // namespace RabbitHole

//------------------------------------------------------------------------------
// This file is part of Rabbit Hole.
//
// Rabbit Hole is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rabbit Hole is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rabbit Hole. If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#pragma once

#include <string>
#include <vector>

class Node;

namespace RabbitHole {
namespace Import {

class ImportResult;

class IImport
{
public:
	// Under what circumstances are we trying to import data?
	enum class Context
	{
		// A node import starts by having the user bring up the
		// context menu on a particular node.
		Node,
		
		// A global import starts by having the user bring up the
		// context menu in the general canvas, without a node or
		// link selected.
		Global
	};

	// Name to be displayed on menus.
	virtual std::string GetName() const = 0;

	// Do we support importing data for a given context?
	virtual bool IsSupported(Context context) const = 0;

	// If we do support Context::Node imports, which type of nodes do
	// we support? Returns a vector containing the internal names of
	// any nodes, e.g. "rabbithole.ipaddress".
	virtual const std::vector<std::string> GetSupportedNodes() const = 0;

	// What file extensions can we import from?
	virtual const std::vector<std::string> GetExtensions() const = 0;

	// Context-sensitive import member functions.
	virtual ImportResult* GlobalImport(const std::string& filename) = 0;
	virtual ImportResult* NodeImport(const std::string& filename, Node* pStartingNode) = 0;
};

} // namespace Import
} // namespace RabbitHole

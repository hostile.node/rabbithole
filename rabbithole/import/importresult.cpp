//------------------------------------------------------------------------------
// This file is part of Rabbit Hole.
//
// Rabbit Hole is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rabbit Hole is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rabbit Hole. If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include "importnode.h"
#include "importresult.h"

namespace RabbitHole {
namespace Import {

//------------------------------------------------------------------------------
ImportResult::ImportResult() :
m_pSourceNode(nullptr)
{

}

//------------------------------------------------------------------------------
ImportResult::~ImportResult()
{
	for (ImportNode* pNode : m_Nodes)
	{
		delete pNode;
	}
}

//------------------------------------------------------------------------------
Node* ImportResult::GetSourceNode() const
{
	return m_pSourceNode;
}

//------------------------------------------------------------------------------
void ImportResult::SetSourceNode(Node* pNode)
{
	m_pSourceNode = pNode;
}

//------------------------------------------------------------------------------
const ImportNodes& ImportResult::GetNodes() const
{
	return m_Nodes;
}

//------------------------------------------------------------------------------
void ImportResult::AddNode(ImportNode* pNode)
{
	m_Nodes.push_back(pNode);
}

} // namespace Import
} // namespace RabbitHole

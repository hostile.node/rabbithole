//------------------------------------------------------------------------------
// This file is part of Rabbit Hole.
//
// Rabbit Hole is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rabbit Hole is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rabbit Hole. If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#pragma once

#include <string>
#include <vector>

class Node;

namespace RabbitHole {
namespace Import {

class ImportNode;
using ImportNodes = std::vector<ImportNode*>;

//------------------------------------------------------------------------------
// ImportResult
// Result of an import operation, containing a graph of any nodes which need to
// be added and their relative source.
//------------------------------------------------------------------------------
class ImportResult
{
public:
	ImportResult();
	~ImportResult();

	// Node from which this import is relative to.
	// Will be null if this result is from a global import.
	Node* GetSourceNode() const;
	void SetSourceNode(Node* pNode);

	// List of nodes (and their respective links) which need
	// to be imported into Rabbit Hole.
	const ImportNodes& GetNodes() const;
	void AddNode(ImportNode* pNode);

private:
	Node* m_pSourceNode;
	ImportNodes m_Nodes;
};

} // namespace Import
} // namespace RabbitHole

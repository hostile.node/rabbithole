//------------------------------------------------------------------------------
// This file is part of Rabbit Hole.
//
// Rabbit Hole is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rabbit Hole is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rabbit Hole. If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include "importnode.h"

namespace RabbitHole {
namespace Import {

class ImportNode;
using ImportLinks = std::vector<ImportNode*>;
using ImportNodes = std::vector<ImportNode*>;

//------------------------------------------------------------------------------
ImportNode::ImportNode(const std::string& internalName, const std::string& displayName, bool parentedToRoot) :
m_InternalName(internalName),
m_DisplayName(displayName),
m_ParentedToRoot(parentedToRoot)
{

}

//------------------------------------------------------------------------------
const std::string& ImportNode::GetInternalName() const
{
	return m_InternalName;
}

//------------------------------------------------------------------------------
const std::string& ImportNode::GetDisplayName() const
{
	return m_DisplayName;
}

//------------------------------------------------------------------------------
const ImportLinks& ImportNode::GetLinks() const
{
	return m_Links;
}

//------------------------------------------------------------------------------
void ImportNode::AddLink(ImportNode* pNode)
{
	m_Links.push_back(pNode);
}

//------------------------------------------------------------------------------
bool ImportNode::IsParentedToRoot() const
{
	return m_ParentedToRoot;
}

} // namespace Import
} // namespace RabbitHole

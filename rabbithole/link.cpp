//------------------------------------------------------------------------------
// This file is part of Rabbit Hole.
//
// Rabbit Hole is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rabbit Hole is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rabbit Hole. If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include "idgenerator.h"
#include "link.h"
#include "log.h"

using json = nlohmann::json;

//------------------------------------------------------------------------------
Link::Link() :
	m_Id(IdGenerator::InvalidId),
	m_StartPinId(IdGenerator::InvalidId),
	m_EndPinId(IdGenerator::InvalidId)
{
}

//------------------------------------------------------------------------------
Link::Link(ed::LinkId id, ed::PinId startPinId, ed::PinId endPinId) :
	m_Id(id),
	m_StartPinId(startPinId),
	m_EndPinId(endPinId)
{
}

//------------------------------------------------------------------------------
ed::LinkId Link::GetId() const
{
	return m_Id;
}

//------------------------------------------------------------------------------
ed::PinId Link::GetStartPinId() const
{
	return m_StartPinId;
}

//------------------------------------------------------------------------------
ed::PinId Link::GetEndPinId() const
{
	return m_EndPinId;
}

//------------------------------------------------------------------------------
json Link::Serialise() const
{
	json j = {
		{ "id", static_cast<unsigned long>(m_Id.Get()) },
		{ "start_pin_id", static_cast<unsigned long>(m_StartPinId.Get()) },
		{ "end_pin_id", static_cast<unsigned long>(m_EndPinId.Get()) }
	};
	return j;
}

//------------------------------------------------------------------------------
bool Link::Deserialise(const json& object)
{
	auto readId = [&](const json& object, const char* key, unsigned int& out) -> bool
	{
		if (object.contains(key) == false)
		{
			Log::Warning("Link::Deserialise failed, couldn't find key '%s'", key);
			return false;
		}

		const json& pair = object[key];
		if (pair.is_number() == false)
		{
			Log::Warning("Link::Deserialise failed, non-numeric ID for '%s'", key);
			return false;
		}

		out = pair.get<unsigned int>();
		IdGenerator::Register(out);
		return true;
	};

	bool success = true;

	unsigned int id = IdGenerator::InvalidId;
	success &= readId(object, "id", id);

	unsigned int startPinId;
	success &= readId(object, "start_pin_id", startPinId);

	unsigned int endPinId;
	success &= readId(object, "end_pin_id", endPinId);

	if (success)
	{
		m_Id = id;
		m_StartPinId = startPinId;
		m_EndPinId = endPinId;
	}

	return success;
}

//------------------------------------------------------------------------------
// This file is part of Rabbit Hole.
//
// Rabbit Hole is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rabbit Hole is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rabbit Hole. If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#pragma once

#include <imgui_node_editor.h>
#include "iserialisable.h"

namespace ed = ax::NodeEditor;

//------------------------------------------------------------------------------
class Link : public ISerialisable
{
public:
	Link();
	Link(ed::LinkId id, ed::PinId startPinId, ed::PinId endPinId);

	ed::LinkId GetId() const;
	ed::PinId GetStartPinId() const;
	ed::PinId GetEndPinId() const;

	// From ISerialisable:
	virtual nlohmann::json Serialise() const override;
	virtual bool Deserialise(const nlohmann::json& object) override;

private:
	ed::LinkId m_Id;
	ed::PinId m_StartPinId;
	ed::PinId m_EndPinId;
};

#include <application.h>
#include <fstream>
#include <iomanip>
#include <list>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <utility>
#include <imgui_node_editor.h>
#include <imgui_file_dlg.h>
#include <ax/Math2D.h>
#include <ax/Builders.h>
#include <ax/Widgets.h>

#define IMGUI_DEFINE_MATH_OPERATORS
#include <imgui_internal.h>

#include <json.hpp>

#include "idgenerator.h"
#include "leftpane.h"
#include "layout.h"
#include "link.h"
#include "log.h"
#include "menubar.h"
#include "rabbithole.h"
#include "import/iimport.h"
#include "import/importnode.h"
#include "import/importresult.h"
#include "import/nmap.h"
#include "node/node.h"
#include "node/nodeinfo.h"
#include "node/nodeeditwindow.h"
#include "node/nodefactory.h"
#include "node/noderegistry.h"

static inline ImRect ImGui_GetItemRect()
{
	return ImRect(ImGui::GetItemRectMin(), ImGui::GetItemRectMax());
}

static inline ImRect ImRect_Expanded(const ImRect& rect, float x, float y)
{
	auto result = rect;
	result.Min.x -= x;
	result.Min.y -= y;
	result.Max.x += x;
	result.Max.y += y;
	return result;
}

namespace ed = ax::NodeEditor;
namespace util = ax::NodeEditor::Utilities;

using namespace ax;

using ax::Widgets::IconType;

static ed::EditorContext* m_Editor = nullptr;

enum class NodeType
{
	Blueprint,
	Simple,
	Tree,
	Comment
};

static std::vector<Node*>   sNodes;
static std::vector<Link>    sLinks;
static ImTextureID          sHeaderBackground = nullptr;
static std::string			sActiveFile;
static bool					sForceNavigateToContent = false;

struct NodeIdLess
{
	bool operator()(const ed::NodeId& lhs, const ed::NodeId& rhs) const
	{
		return lhs.AsPointer() < rhs.AsPointer();
	}
};

static Node* FindNode(ed::NodeId id)
{
	for (auto& node : sNodes)
		if (node->GetId() == id)
			return node;

	return nullptr;
}

static Link* FindLink(ed::LinkId id)
{
	for (auto& link : sLinks)
		if (link.GetId() == id)
			return &link;

	return nullptr;
}

static Pin* FindPin(ed::PinId id)
{
	if (!id)
	{
		return nullptr;
	}

	for (auto& node : sNodes)
	{
		if (node->GetPin().ID == id)
		{
			return &node->GetPin();
		}
	}

	return nullptr;
}

static bool IsPinLinked(ed::PinId id)
{
	if (!id)
		return false;

	for (auto& link : sLinks)
		if (link.GetStartPinId() == id || link.GetEndPinId() == id)
			return true;

	return false;
}

static bool CanCreateLink(Pin* a, Pin* b)
{
	if (!a || !b || a == b || a->Kind == b->Kind || a->Type != b->Type || a->Node == b->Node)
	{
		return false;
	}

	return true;
}

static Node* SpawnNode(const std::string& internalName)
{
	Node* pNode = NodeFactory::Create(internalName);
	assert(pNode != nullptr);
	if (pNode == nullptr)
	{
		return nullptr;
	}
	else
	{
		sNodes.push_back(pNode);
		return pNode;
	}
}

namespace RabbitHole
{

void SetUserInterfaceStyle();

std::vector<Import::IImport*> sImporters;

void Initialise()
{
	Log::AddLogTarget(std::make_shared<TTYLogger>());
	Log::AddLogTarget(std::make_shared<FileLogger>("log.txt"));

#ifdef _WIN32
	Log::AddLogTarget(std::make_shared<VisualStudioLogger>());
#endif

	SetUserInterfaceStyle();
	NodeRegistry::Initialise();
	LeftPane::Initialise(); // Must be initialised after the NodeRegistry.
	NewFile();

	sHeaderBackground = Application_LoadTexture("Data/BlueprintBackground.png");

	sImporters.push_back(new Import::Nmap());
}

void Shutdown()
{
	auto releaseTexture = [](ImTextureID& id)
	{
		if (id)
		{
			Application_DestroyTexture(id);
			id = nullptr;
		}
	};

	releaseTexture(sHeaderBackground);

	if (m_Editor)
	{
		ed::DestroyEditor(m_Editor);
		m_Editor = nullptr;
	}

	for (Node* pNode : sNodes)
	{
		delete pNode;
	}

	for (Import::IImport* pImporter : sImporters)
	{
		delete pImporter;
	}

	NodeRegistry::Shutdown();
}

void NewFile()
{
	if (m_Editor != nullptr)
	{
		ed::DestroyEditor(m_Editor);
	}

	ed::Config config;
	config.SettingsFile = "rabbithole.json";
	m_Editor = ed::CreateEditor(&config);
	ed::SetCurrentEditor(m_Editor);

	for (Node* pNode : sNodes)
	{
		delete pNode;
	}
	sNodes.clear();
	sLinks.clear();

	sActiveFile = "";
}

bool OpenFile(const std::string& filename)
{
	using nlohmann::json;
	std::ifstream file(filename);
	if (file.good() == false)
	{
		Log::Warning("Couldn't open file: '%s'", filename.c_str());
		return false;
	}

	// Gives us a blank slate to work on.
	NewFile();

	json j;
	file >> j;

	json nodes = j["Nodes"];
	if (nodes.is_array())
	{
		sNodes.reserve(nodes.size());
		for (json nodeData : nodes)
		{
			Node* pNode = new Node();
			bool result = pNode->Deserialise(nodeData);
			if (result)
			{
				sNodes.push_back(pNode);
			}
			else
			{
				Log::Warning("Couldn't deserialise node, skipping...");
				delete pNode;
			}
		}
	}

	json links = j["Links"];
	if (links.is_array())
	{
		sLinks.reserve(links.size());
		for (json linkData : links)
		{
			Link link;
			bool result = link.Deserialise(linkData);
			if (result)
			{
				sLinks.push_back(link);
			}
			else
			{
				Log::Warning("Couldn't deserialise link, skipping...");
			}
		}
	}

	// We can't call ed::NavigateToContent() at this point, as the nodes aren't
	// fully built yet. This gets postponed until after the render happens.
	sForceNavigateToContent = true;

	sActiveFile = filename;

	Log::Info("Opened file '%s'.", filename.c_str());

	return true;
}

bool SaveFile(const std::string& filename)
{
	using nlohmann::json;
	json j;

	json nodes = json::array();
	for (Node* pNode : sNodes)
	{
		nodes.push_back(pNode->Serialise());
	}
	j["Nodes"] = nodes;

	json links = json::array();
	for (const Link& link : sLinks)
	{
		links.push_back(link.Serialise());
	}
	j["Links"] = links;

	std::ofstream file(filename);
	if (file.good())
	{
		file << std::setw(4) << j << std::endl;
		Log::Info("Saved file '%s'.", filename.c_str());
		return true;
	}
	else
	{
		Log::Warning("Failed to save file '%s'.", filename.c_str());
		return false;
	}
}

void Quit()
{
	Application_Quit();
}

void ImportNodes(Import::ImportResult* pResult)
{
	if (pResult == nullptr)
	{
		return;
	}

	using namespace Import;

	struct SpawnedNode
	{
		SpawnedNode(Import::ImportNode* pImportNode, Node* pSpawnNode)
		{
			pImportedNode = pImportNode;
			pSpawnedNode = pSpawnNode;
		}

		Import::ImportNode* pImportedNode;
		Node* pSpawnedNode;
	};
	std::vector<SpawnedNode> spawnedNodes;

	for (Import::ImportNode* pImportNode : pResult->GetNodes())
	{
		Node* pSpawnedNode = SpawnNode(pImportNode->GetInternalName());
		pSpawnedNode->SetText(pImportNode->GetDisplayName());
		spawnedNodes.emplace_back(pImportNode, pSpawnedNode);
	}

	auto findSpawnedNode = [&spawnedNodes](const Import::ImportNode* pImportNode) -> SpawnedNode*
	{
		std::size_t numSpawnedNodes = spawnedNodes.size();
		for (std::size_t i = 0; i < numSpawnedNodes; ++i)
		{
			if (spawnedNodes[i].pImportedNode == pImportNode)
			{
				return &spawnedNodes[i];
			}
		}
		return nullptr;
	};

	
	std::vector<Node*> nodesToLayout;
	std::vector<Link> linksToLayout;

	nodesToLayout.reserve(spawnedNodes.size() + 1);
	if (pResult->GetSourceNode() != nullptr)
	{
		nodesToLayout.push_back(pResult->GetSourceNode());
	}

	for (SpawnedNode& spawnedNode : spawnedNodes)
	{
		for (const Import::ImportNode* pLinkWith : spawnedNode.pImportedNode->GetLinks())
		{
			SpawnedNode* pLinkedWith = findSpawnedNode(pLinkWith);
			if (pLinkedWith != nullptr)
			{
				Link link(IdGenerator::Next(), spawnedNode.pSpawnedNode->GetPin().ID, pLinkedWith->pSpawnedNode->GetPin().ID);
				sLinks.push_back(link);
				linksToLayout.push_back(link);
			}
		}

		if (pResult->GetSourceNode() != nullptr && spawnedNode.pImportedNode->IsParentedToRoot())
		{
			Link link(IdGenerator::Next(), pResult->GetSourceNode()->GetPin().ID, spawnedNode.pSpawnedNode->GetPin().ID);
			sLinks.push_back(link);
			linksToLayout.push_back(link);
		}

		nodesToLayout.push_back(spawnedNode.pSpawnedNode);

		ed::SetNodePosition(spawnedNode.pSpawnedNode->GetId(), ed::GetNodePosition(pResult->GetSourceNode()->GetId()));
	}

	Layout::Perform(nodesToLayout, linksToLayout, pResult->GetSourceNode());

	delete pResult;
}

void DrawNodeEditWindows()
{
	static std::list<NodeEditWindow> sOpenWindows;
	NodeEditWindow* pWindowToClose = nullptr;

	// If we have double-clicked a pin, see if we need to open a new window to edit the node.
	Pin* pPin = FindPin(ed::GetDoubleClickedPin());
	if (pPin != nullptr)
	{
		bool found = false;
		Node* pNode = pPin->Node;
		for (const NodeEditWindow& window : sOpenWindows)
		{
			if (window.GetNode() == pNode)
			{
				found = true;
				break;
			}
		}

		if (found == false)
		{
			sOpenWindows.push_back(NodeEditWindow(pPin->Node, ImGui::GetMousePos()));
		}
	}

	for (NodeEditWindow& window : sOpenWindows)
	{
		window.Draw();

		if (window.IsOpen() == false)
		{
			pWindowToClose = &window;
		}
	}

	if (pWindowToClose != nullptr)
	{
		sOpenWindows.remove(*pWindowToClose);
	}
}

void DrawNodeContextMenu()
{
	// Figure out if we want to open up the context menu. 
	// We can only do so if at least one importer is valid for this particular node.
	static ed::PinId contextPinId = 0;
	static Node* pSelectedNode = nullptr;
	static std::vector<Import::IImport*> importOptions;
	ImVec2 openPopupPosition = ImGui::GetMousePos();
	if (ed::ShowPinContextMenu(&contextPinId))
	{
		pSelectedNode = FindPin(contextPinId)->Node;

		// Do we have any importers which can be applied on this node?
		importOptions.clear();
		for (Import::IImport* pImporter : sImporters)
		{
			const std::vector<std::string>& supportedNodes = pImporter->GetSupportedNodes();
			if (std::find(supportedNodes.cbegin(), supportedNodes.cend(), pSelectedNode->GetNodeInfo()->GetInternalName()) != supportedNodes.cend() &&
				pImporter->IsSupported(Import::IImport::Context::Node))
			{
				importOptions.push_back(pImporter);
			}
		}

		if (importOptions.empty() == false)
		{
			ImGui::OpenPopup("Node Context Menu");
		}
	}

	// Display the context menu.
	static Import::IImport* pSelectedImporter = nullptr;
	if (ImGui::BeginPopup("Node Context Menu"))
	{
		if (importOptions.empty() == false)
		{
			if (ImGui::BeginMenu("Import"))
			{
				for (Import::IImport* pImporter : importOptions)
				{
					if (ImGui::MenuItem(pImporter->GetName().c_str()))
					{
						pSelectedImporter = pImporter;
					}
				}

				ImGui::EndMenu();
			}
		}

		ImGui::EndPopup();
	}

	if (pSelectedImporter != nullptr && ImGui::FileDialog("Open file", pSelectedImporter->GetExtensions()))
	{
		if (ImGui::FileDialogAccepted() && ImGui::FileDialogFilename().empty() == false)
		{
			ImportNodes(pSelectedImporter->NodeImport(ImGui::FileDialogFullPath(), pSelectedNode));
			ImGui::FileDialogReset();
		}

		pSelectedImporter = nullptr;
	}
}

void SetUserInterfaceStyle()
{
	ImGuiStyle &st = ImGui::GetStyle();
	st.FrameBorderSize = 1.0f;
	st.FramePadding = ImVec2(4.0f, 2.0f);
	st.ItemSpacing = ImVec2(8.0f, 2.0f);
	st.WindowBorderSize = 1.0f;
	st.WindowRounding = 1.0f;
	st.ChildRounding = 1.0f;
	st.FrameRounding = 1.0f;
	st.ScrollbarRounding = 1.0f;
	st.GrabRounding = 1.0f;

	// Setup style
	ImVec4* colors = ImGui::GetStyle().Colors;
	colors[ImGuiCol_Text] = ImVec4(1.00f, 1.00f, 1.00f, 0.95f);
	colors[ImGuiCol_TextDisabled] = ImVec4(0.50f, 0.50f, 0.50f, 1.00f);
	colors[ImGuiCol_WindowBg] = ImVec4(0.13f, 0.12f, 0.12f, 0.90f);
	colors[ImGuiCol_ChildBg] = ImVec4(1.00f, 1.00f, 1.00f, 0.00f);
	colors[ImGuiCol_PopupBg] = ImVec4(0.05f, 0.05f, 0.05f, 0.94f);
	colors[ImGuiCol_Border] = ImVec4(0.53f, 0.53f, 0.53f, 0.46f);
	colors[ImGuiCol_BorderShadow] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
	colors[ImGuiCol_FrameBg] = ImVec4(0.00f, 0.00f, 0.00f, 0.85f);
	colors[ImGuiCol_FrameBgHovered] = ImVec4(0.22f, 0.22f, 0.22f, 0.40f);
	colors[ImGuiCol_FrameBgActive] = ImVec4(0.16f, 0.16f, 0.16f, 0.53f);
	colors[ImGuiCol_TitleBg] = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
	colors[ImGuiCol_TitleBgActive] = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
	colors[ImGuiCol_TitleBgCollapsed] = ImVec4(0.00f, 0.00f, 0.00f, 0.51f);
	colors[ImGuiCol_MenuBarBg] = ImVec4(0.12f, 0.12f, 0.12f, 1.00f);
	colors[ImGuiCol_ScrollbarBg] = ImVec4(0.02f, 0.02f, 0.02f, 0.53f);
	colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.31f, 0.31f, 0.31f, 1.00f);
	colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.41f, 0.41f, 0.41f, 1.00f);
	colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.48f, 0.48f, 0.48f, 1.00f);
	colors[ImGuiCol_CheckMark] = ImVec4(0.79f, 0.79f, 0.79f, 1.00f);
	colors[ImGuiCol_SliderGrab] = ImVec4(0.48f, 0.47f, 0.47f, 0.91f);
	colors[ImGuiCol_SliderGrabActive] = ImVec4(0.56f, 0.55f, 0.55f, 0.62f);
	colors[ImGuiCol_Button] = ImVec4(0.50f, 0.50f, 0.50f, 0.63f);
	colors[ImGuiCol_ButtonHovered] = ImVec4(0.67f, 0.67f, 0.68f, 0.63f);
	colors[ImGuiCol_ButtonActive] = ImVec4(0.26f, 0.26f, 0.26f, 0.63f);
	colors[ImGuiCol_Header] = ImVec4(0.54f, 0.54f, 0.54f, 0.58f);
	colors[ImGuiCol_HeaderHovered] = ImVec4(0.64f, 0.65f, 0.65f, 0.80f);
	colors[ImGuiCol_HeaderActive] = ImVec4(0.25f, 0.25f, 0.25f, 0.80f);
	colors[ImGuiCol_Separator] = ImVec4(0.58f, 0.58f, 0.58f, 0.50f);
	colors[ImGuiCol_SeparatorHovered] = ImVec4(0.81f, 0.81f, 0.81f, 0.64f);
	colors[ImGuiCol_SeparatorActive] = ImVec4(0.81f, 0.81f, 0.81f, 0.64f);
	colors[ImGuiCol_ResizeGrip] = ImVec4(0.87f, 0.87f, 0.87f, 0.53f);
	colors[ImGuiCol_ResizeGripHovered] = ImVec4(0.87f, 0.87f, 0.87f, 0.74f);
	colors[ImGuiCol_ResizeGripActive] = ImVec4(0.87f, 0.87f, 0.87f, 0.74f);
	colors[ImGuiCol_PlotLines] = ImVec4(0.61f, 0.61f, 0.61f, 1.00f);
	colors[ImGuiCol_PlotLinesHovered] = ImVec4(0.68f, 0.68f, 0.68f, 1.00f);
	colors[ImGuiCol_PlotHistogram] = ImVec4(0.90f, 0.77f, 0.33f, 1.00f);
	colors[ImGuiCol_PlotHistogramHovered] = ImVec4(0.87f, 0.55f, 0.08f, 1.00f);
	colors[ImGuiCol_TextSelectedBg] = ImVec4(0.47f, 0.60f, 0.76f, 0.47f);
	colors[ImGuiCol_DragDropTarget] = ImVec4(0.58f, 0.58f, 0.58f, 0.90f);
	colors[ImGuiCol_NavHighlight] = ImVec4(0.60f, 0.60f, 0.60f, 1.00f);
	colors[ImGuiCol_NavWindowingHighlight] = ImVec4(1.00f, 1.00f, 1.00f, 0.70f);
}

void HandleDragDrop()
{
	if (ImGui::BeginDragDropTarget())
	{
		if (const ImGuiPayload* pPayload = ImGui::AcceptDragDropPayload("DND_NODE", ImGuiDragDropFlags_AcceptNoDrawDefaultRect))
		{
			IM_ASSERT(pPayload->DataSize == sizeof(NodeInfo**));
			NodeInfo* pNodeInfo = *reinterpret_cast<NodeInfo**>(pPayload->Data);
			Node* pNode = SpawnNode(pNodeInfo->GetInternalName());
			if (pNode != nullptr)
			{
				ed::SetNodePosition(pNode->GetId(), ed::ScreenToCanvas(ImGui::GetMousePos()));
			}
		}
		ImGui::EndDragDropTarget();
	}
}

}

const char* Application_GetName()
{
	return "Rabbit Hole";
}

void Application_Initialize()
{
	RabbitHole::Initialise();
}

void Application_Finalize()
{
	RabbitHole::Shutdown();
}

static bool Splitter(bool split_vertically, float thickness, float* size1, float* size2, float min_size1, float min_size2, float splitter_long_axis_size = -1.0f)
{
	using namespace ImGui;
	ImGuiContext& g = *GImGui;
	ImGuiWindow* window = g.CurrentWindow;
	ImGuiID id = window->GetID("##Splitter");
	ImRect bb;
	bb.Min = window->DC.CursorPos + (split_vertically ? ImVec2(*size1, 0.0f) : ImVec2(0.0f, *size1));
	bb.Max = bb.Min + CalcItemSize(split_vertically ? ImVec2(thickness, splitter_long_axis_size) : ImVec2(splitter_long_axis_size, thickness), 0.0f, 0.0f);
	return SplitterBehavior(bb, id, split_vertically ? ImGuiAxis_X : ImGuiAxis_Y, size1, size2, min_size1, min_size2, 0.0f);
}

void Application_Frame()
{
	ed::SetCurrentEditor(m_Editor);
	RabbitHole::Layout::Advance(1.0f / 30.0f);

	Menubar::Draw();

	static bool createNewNode = false;
	static Pin* newNodeLinkPin = nullptr;
	static Pin* newLinkPin = nullptr;

	static float leftPaneWidth = 300.0f;
	static float rightPaneWidth = 900.0f;
	Splitter(true, 4.0f, &leftPaneWidth, &rightPaneWidth, 50.0f, 50.0f);

	LeftPane::Draw(leftPaneWidth - 4.0f);

	ImGui::SameLine(0.0f, 12.0f);

	ed::Begin("Node editor");
	{
		auto cursorTopLeft = ImGui::GetCursorScreenPos();

		util::BlueprintNodeBuilder builder(sHeaderBackground, Application_GetTextureWidth(sHeaderBackground), Application_GetTextureHeight(sHeaderBackground));

		ed::PushStyleColor(ed::StyleColor_NodeBg, ImColor(128, 128, 128, 0));
		ed::PushStyleColor(ed::StyleColor_NodeBorder, ImColor(32, 32, 32, 0));
		ed::PushStyleColor(ed::StyleColor_PinRect, ImColor(60, 180, 255, 150));
		ed::PushStyleColor(ed::StyleColor_PinRectBorder, ImColor(60, 180, 255, 150));

		ed::PushStyleVar(ed::StyleVar_NodePadding, ImVec4(0, 0, 0, 0));
		ed::PushStyleVar(ed::StyleVar_NodeRounding, 0.0f);
		ed::PushStyleVar(ed::StyleVar_SourceDirection, ImVec2(0.0f, 1.0f));
		ed::PushStyleVar(ed::StyleVar_TargetDirection, ImVec2(0.0f, -1.0f));
		ed::PushStyleVar(ed::StyleVar_LinkStrength, 0.0f);
		ed::PushStyleVar(ed::StyleVar_PinBorderWidth, 1.0f);
		ed::PushStyleVar(ed::StyleVar_PinRadius, 5.0f);

		for (auto& node : sNodes)
		{
			ed::BeginNode(node->GetId());

			ImGui::BeginVertical(node->GetId().AsPointer());

			ImGui::BeginHorizontal("content_frame");

			ImGui::BeginVertical("content", ImVec2(0.0f, 0.0f));
			ImGui::Dummy(ImVec2(64, 0));
			ImGui::Spring(1);
			if (node->GetIcon() != nullptr)
			{
				ImGui::Image(node->GetIcon(), ImVec2(48, 48));
			}
			ImGui::Spring(1);
			ImGui::TextUnformatted(node->GetText().c_str());
			ImGui::Spring(1);
			ImGui::EndVertical();
			auto contentRect = ImGui_GetItemRect();

			int outputAlpha = 200;
			Pin& pin = node->GetPin();

			ImGui::Spring(1, 0);

			ed::PushStyleVar(ed::StyleVar_PinCorners, 3);
			ed::BeginPin(pin.ID, ed::PinKind::Output);
			ed::PinPivotRect(contentRect.GetTL(), contentRect.GetBR());
			ed::PinRect(contentRect.GetTL(), contentRect.GetBR());
			ed::EndPin();
			ed::PopStyleVar();

			if (newLinkPin && !CanCreateLink(newLinkPin, &pin) && &pin != newLinkPin)
				outputAlpha = (int)(255 * ImGui::GetStyle().Alpha * (48.0f / 255.0f));

			ImGui::EndHorizontal();

			ImGui::EndVertical();

			ed::EndNode();
		}

		ed::PopStyleVar(7);
		ed::PopStyleColor(4);

		ImColor linkColor(1.0f, 1.0f, 1.0f, 1.0f);
		for (auto& link : sLinks)
		{
			ed::Link(link.GetId(), link.GetStartPinId(), link.GetEndPinId(), linkColor, 2.0f);
		}

		if (!createNewNode)
		{
			if (ed::BeginCreate(ImColor(255, 255, 255), 2.0f))
			{
				auto showLabel = [](const char* label, ImColor color)
				{
					ImGui::SetCursorPosY(ImGui::GetCursorPosY() - ImGui::GetTextLineHeight());
					auto size = ImGui::CalcTextSize(label);

					auto padding = ImGui::GetStyle().FramePadding;
					auto spacing = ImGui::GetStyle().ItemSpacing;

					ImGui::SetCursorPos(ImGui::GetCursorPos() + ImVec2(spacing.x, -spacing.y));

					auto rectMin = ImGui::GetCursorScreenPos() - padding;
					auto rectMax = ImGui::GetCursorScreenPos() + size + padding;

					auto drawList = ImGui::GetWindowDrawList();
					drawList->AddRectFilled(rectMin, rectMax, color, size.y * 0.15f);
					ImGui::TextUnformatted(label);
				};

				ed::PinId startPinId = 0, endPinId = 0;
				if (ed::QueryNewLink(&startPinId, &endPinId))
				{
					auto startPin = FindPin(startPinId);
					auto endPin = FindPin(endPinId);

					newLinkPin = startPin ? startPin : endPin;

					if (startPin->Kind == PinKind::Input)
					{
						std::swap(startPin, endPin);
						std::swap(startPinId, endPinId);
					}

					if (startPin && endPin)
					{
						bool canCreateLink = true;
						if (endPin == startPin)
						{
							ed::RejectNewItem(ImColor(255, 0, 0), 2.0f);
							canCreateLink = false;
						}
						else if (endPin->Node == startPin->Node)
						{
							showLabel("Cannot connect to self.", ImColor(45, 32, 32, 180));
							ed::RejectNewItem(ImColor(255, 0, 0), 1.0f);
							canCreateLink = false;
						}
						else
						{
							for (auto& link : sLinks)
							{
								if (link.GetStartPinId() == startPin->ID && link.GetEndPinId() == endPin->ID ||
									link.GetStartPinId() == endPin->ID && link.GetEndPinId() == startPin->ID)
								{
									showLabel("Link already exists.", ImColor(45, 32, 32, 180));
									ed::RejectNewItem(ImColor(255, 0, 0), 1.0f);
									canCreateLink = false;
									break;
								}
							}
						}

						if (canCreateLink)
						{
							showLabel("+ Create Link", ImColor(32, 45, 32, 180));
							if (ed::AcceptNewItem(ImColor(128, 255, 128), 4.0f))
							{
								sLinks.emplace_back(Link(IdGenerator::Next(), startPinId, endPinId));
							}
						}
					}
				}

				ed::PinId pinId = 0;
				if (ed::QueryNewNode(&pinId))
				{
					newLinkPin = FindPin(pinId);
					if (newLinkPin)
						showLabel("+ Create Node", ImColor(32, 45, 32, 180));

					if (ed::AcceptNewItem())
					{
						createNewNode = true;
						newNodeLinkPin = FindPin(pinId);
						newLinkPin = nullptr;
						ed::Suspend();
						ImGui::OpenPopup("Create New Node");
						ed::Resume();
					}
				}
			}
			else
				newLinkPin = nullptr;

			ed::EndCreate();

			if (ed::BeginDelete())
			{
				ed::LinkId linkId = 0;
				while (ed::QueryDeletedLink(&linkId))
				{
					if (ed::AcceptDeletedItem())
					{
						auto id = std::find_if(sLinks.begin(), sLinks.end(), [linkId](auto& link) { return link.GetId() == linkId; });
						if (id != sLinks.end())
							sLinks.erase(id);
					}
				}

				ed::NodeId nodeId = 0;
				while (ed::QueryDeletedNode(&nodeId))
				{
					if (ed::AcceptDeletedItem())
					{
						auto id = std::find_if(sNodes.begin(), sNodes.end(), [nodeId](auto& node) { return node->GetId() == nodeId; });
						if (id != sNodes.end())
							sNodes.erase(id);
					}
				}
			}
			ed::EndDelete();
		}

		ImGui::SetCursorScreenPos(cursorTopLeft);
	}

	auto openPopupPosition = ImGui::GetMousePos();
	ed::Suspend();
	if (ed::ShowBackgroundContextMenu())
	{
		ImGui::OpenPopup("Create New Node");
		newNodeLinkPin = nullptr;
	}

	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(8, 8));
	if (ImGui::BeginPopup("Create New Node"))
	{
		auto newNodePostion = openPopupPosition;

		Node* node = nullptr;
		float sz = ImGui::GetTextLineHeight();
		const NodeInfos& nodeInfos = NodeRegistry::Get();
		for (NodeInfo* pNodeInfo : nodeInfos)
		{
			const char* name = pNodeInfo->GetDisplayName().c_str();
			ImVec2 p = ImGui::GetCursorScreenPos();
			bool selected = false;
			ImGui::PushID(pNodeInfo->GetDisplayName().c_str());
			ImGui::Selectable("", &selected); ImGui::SameLine();
			if (pNodeInfo->GetIcon() != nullptr)
			{
				ImGui::GetWindowDrawList()->AddImage(pNodeInfo->GetIcon(), p, ImVec2(p.x+sz, p.y+sz));
				ImGui::SameLine();
			}
			ImGui::Dummy(ImVec2(sz, sz));
			ImGui::SameLine();
			ImGui::Text("%s", name);
			ImGui::PopID();

			if (selected)
			{
				node = SpawnNode(pNodeInfo->GetInternalName());
			}
		}

		if (node)
		{
			createNewNode = false;

			ed::SetNodePosition(node->GetId(), newNodePostion);

			if (auto startPin = newNodeLinkPin)
			{
				Pin& pin = node->GetPin();
				if (CanCreateLink(startPin, &pin))
				{
					auto endPin = &pin;
					if (startPin->Kind == PinKind::Input)
						std::swap(startPin, endPin);

					sLinks.emplace_back(Link(IdGenerator::Next(), startPin->ID, endPin->ID));
				}
			}
		}

		ImGui::EndPopup();
	}
	else
		createNewNode = false;

	RabbitHole::DrawNodeContextMenu();

	ImGui::PopStyleVar();
	ed::Resume();
	ed::End();

	RabbitHole::DrawNodeEditWindows();
	RabbitHole::HandleDragDrop();

	if (sForceNavigateToContent)
	{
		ed::NavigateToContent();
		sForceNavigateToContent = false;
	}

	//ImGui::ShowTestWindow();
	//ImGui::ShowMetricsWindow();
}

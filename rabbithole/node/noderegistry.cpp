//------------------------------------------------------------------------------
// This file is part of Rabbit Hole.
//
// Rabbit Hole is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rabbit Hole is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rabbit Hole. If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <algorithm>
#include <unordered_map>

#include "nodeinfo.h"
#include "noderegistry.h"

//------------------------------------------------------------------------------
// The NodeRegistry keeps two data structures: a vector for quick iteration and
// an unordered map for quick finds.
//------------------------------------------------------------------------------

using NodeRegistryMap = std::unordered_map<std::string, NodeInfo*>;
static NodeRegistryMap sNodeRegistryMap;
static NodeInfos sNodeRegistryVector;

//------------------------------------------------------------------------------
/* static */ void NodeRegistry::Initialise()
{
	std::size_t reserveCount = 16u;
	sNodeRegistryMap.reserve(reserveCount);
	sNodeRegistryVector.reserve(reserveCount);

	// TODO: Read from a folder.

	// Pentesting
	sNodeRegistryVector.push_back(new NodeInfo("rabbithole.ipaddress", "IP Address", "127.0.0.1", "Pentesting", "Data/icons8-network-card-48.png"));
	sNodeRegistryVector.push_back(new NodeInfo("rabbithole.port", "Port", "80", "Pentesting", "Data/icons8-wired-network-48.png"));
	sNodeRegistryVector.push_back(new NodeInfo("rabbithole.service", "Service", "HTTP", "Pentesting", "Data/icons8-services-48.png"));
	sNodeRegistryVector.push_back(new NodeInfo("rabbithole.user", "Username", "User", "Pentesting", "Data/icons8-name-48.png"));
	sNodeRegistryVector.push_back(new NodeInfo("rabbithole.file", "File", "Filename", "Pentesting", "Data/icons8-file-48.png"));
	sNodeRegistryVector.push_back(new NodeInfo("rabbithole.webpage", "Webpage", "www.google.com", "Pentesting", "Data/icons8-website-48.png"));
	sNodeRegistryVector.push_back(new NodeInfo("rabbithole.password", "Password", "*****", "Pentesting", "Data/icons8-password-48.png"));
	sNodeRegistryVector.push_back(new NodeInfo("rabbithole.share", "Share", "C$", "Pentesting", "Data/icons8-shared-folder-48.png"));

	// General
	sNodeRegistryVector.push_back(new NodeInfo("rabbithole.note", "Note", "Note", "General", "Data/icons8-note-48.png"));
	sNodeRegistryVector.push_back(new NodeInfo("rabbithole.question", "Question", "?", "General", "Data/icons8-question-mark-48.png"));

	// OSINT
	// Person
	// Email

	// Sort by name - this makes displaying the node types easier, as the sort just has to happen once.
	std::sort(sNodeRegistryVector.begin(), sNodeRegistryVector.end(),
		[](NodeInfo* pNodeA, NodeInfo* pNodeB) -> bool
		{
			return pNodeA->GetDisplayName() < pNodeB->GetDisplayName();
		}	
	);
	
	for (NodeInfo* pNodeInfo : sNodeRegistryVector)
	{
		sNodeRegistryMap[pNodeInfo->GetInternalName()] = pNodeInfo;
	}
}

//------------------------------------------------------------------------------
/* static */ void NodeRegistry::Shutdown()
{
	for (NodeInfo* pNodeInfo : sNodeRegistryVector)
	{
		delete pNodeInfo;
	}
}

//------------------------------------------------------------------------------
/* static */ NodeInfo* NodeRegistry::Find(const std::string& internalName)
{
	NodeRegistryMap::const_iterator it = sNodeRegistryMap.find(internalName);
	return (it == sNodeRegistryMap.cend()) ? nullptr : it->second;
}

//------------------------------------------------------------------------------
/* static */ const NodeInfos& NodeRegistry::Get()
{
	return sNodeRegistryVector;
}

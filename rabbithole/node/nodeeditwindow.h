//------------------------------------------------------------------------------
// This file is part of Rabbit Hole.
//
// Rabbit Hole is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rabbit Hole is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rabbit Hole. If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#pragma once

#include <string>
#include "imgui.h"

class Node;

//------------------------------------------------------------------------------
// NodeEditWindow
// 
//------------------------------------------------------------------------------
class NodeEditWindow
{
public:
	NodeEditWindow(Node* pNode, const ImVec2& position);

	bool operator==(const NodeEditWindow& window);

	void Draw();
	Node* GetNode() const;
	bool IsOpen() const;

private:
	static const size_t sBufferSize = 64;
	char m_Buffer[sBufferSize];
	Node* m_pNode;
	ImVec2 m_StartingPosition;
	bool m_StartingPositionSet;
	bool m_IsOpen;
};

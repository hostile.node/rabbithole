//------------------------------------------------------------------------------
// This file is part of Rabbit Hole.
//
// Rabbit Hole is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rabbit Hole is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rabbit Hole. If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include "node.h"
#include "nodeinfo.h"
#include "nodefactory.h"
#include "noderegistry.h"

class Node;

static unsigned int sNodeId = 0u;

//------------------------------------------------------------------------------
/* static */ Node* NodeFactory::Create(const std::string& internalName)
{
	NodeInfo* pNodeInfo = NodeRegistry::Find(internalName);
	if (pNodeInfo != nullptr)
	{
		return new Node(pNodeInfo); 
	}
	else
	{
		return nullptr;
	}
}


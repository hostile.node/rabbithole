//------------------------------------------------------------------------------
// This file is part of Rabbit Hole.
//
// Rabbit Hole is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rabbit Hole is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rabbit Hole. If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <application.h>
#include "nodeinfo.h"
#include "../log.h"

//------------------------------------------------------------------------------
NodeInfo::NodeInfo(const std::string& internalName, const std::string& displayName, const std::string& defaultText, const std::string& category, const std::string& icon) :
	m_InternalName(internalName),
	m_DisplayName(displayName),
	m_DefaultText(defaultText),
	m_Category(category),
	m_pIcon(nullptr)
{
	m_pIcon = Application_LoadTexture(icon.c_str());

	if (m_pIcon == nullptr)
	{
		Log::Warning("Couldn't load icon: '%s'", icon.c_str());
	}
}

//------------------------------------------------------------------------------
NodeInfo::~NodeInfo()
{
	if (m_pIcon != nullptr)
	{
		Application_DestroyTexture(m_pIcon);
	}
}

//------------------------------------------------------------------------------
const std::string& NodeInfo::GetInternalName() const
{
	return m_InternalName;
}

//------------------------------------------------------------------------------
const std::string& NodeInfo::GetDisplayName() const
{
	return m_DisplayName;
}

//------------------------------------------------------------------------------
const std::string& NodeInfo::GetDefaultText() const
{
	return m_DefaultText;
}

//------------------------------------------------------------------------------
const std::string& NodeInfo::GetCategory() const
{
	return m_Category;
}

//------------------------------------------------------------------------------
void* NodeInfo::GetIcon() const
{
	return m_pIcon;
}

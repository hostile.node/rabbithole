#include <string>

namespace RabbitHole
{
	void NewFile();
	bool OpenFile(const std::string& filename);
	bool SaveFile(const std::string& filename);
	void Quit();
}
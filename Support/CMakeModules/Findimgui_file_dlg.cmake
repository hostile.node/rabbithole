
if (TARGET imgui_file_dlg)
    return()
endif()

set(_imgui_file_dlg_SourceDir ${CMAKE_SOURCE_DIR}/ext/imgui_file_dlg)
set(_imgui_file_dlg_BinaryDir ${CMAKE_BINARY_DIR}/ext/imgui_file_dlg)

add_subdirectory(${_imgui_file_dlg_SourceDir} ${_imgui_file_dlg_BinaryDir})

include(${CMAKE_ROOT}/Modules/FindPackageHandleStandardArgs.cmake)

find_package_handle_standard_args(
    imgui_file_dlg
    REQUIRED_VARS
        _imgui_file_dlg_SourceDir
)


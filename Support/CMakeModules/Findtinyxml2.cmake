
if (TARGET tinyxml2)
    return()
endif()

set(_tinyxml2_SourceDir ${CMAKE_SOURCE_DIR}/ext/tinyxml2)
set(_tinyxml2_BinaryDir ${CMAKE_BINARY_DIR}/ext/tinyxml2)

add_subdirectory(${_tinyxml2_SourceDir} ${_tinyxml2_BinaryDir})

include(${CMAKE_ROOT}/Modules/FindPackageHandleStandardArgs.cmake)

find_package_handle_standard_args(
    tinyxml2
    REQUIRED_VARS
        _tinyxml2_SourceDir
)

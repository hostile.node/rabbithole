
if (TARGET glfw3)
    return()
endif()

set(_glfw3_SourceDir ${CMAKE_SOURCE_DIR}/ext/glfw-3.3)
set(_glfw3_BinaryDir ${CMAKE_BINARY_DIR}/ext/glfw-3.3)

add_subdirectory(${_glfw3_SourceDir} ${_glfw3_BinaryDir})

include(${CMAKE_ROOT}/Modules/FindPackageHandleStandardArgs.cmake)

find_package_handle_standard_args(
    glfw3
    REQUIRED_VARS
        _glfw3_SourceDir
)


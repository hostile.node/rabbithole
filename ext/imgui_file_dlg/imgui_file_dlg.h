// From https://github.com/aiekick/ImGuiFileDialog
//
// Modified by AlastairGrowcott@yahoo.com to work on Posix systems.
// Further modified by pnunes515@protonmail.com to work on Windows systems as well.

#ifndef __IMGUI_FILE_DIALOG_H_
#define __IMGUI_FILE_DIALOG_H_


#include <cstdbool>
#include <vector>
#include <string>


namespace ImGui
{
	bool FileDialog(const char  *vName,
		std::vector<std::string> vFilters = {},
		std::string vPath = ".",
		std::string vDefaultFileName = "");

	bool FileDialogAccepted();

	std::string FileDialogFullPath();
	std::string FileDialogPath();
	std::string FileDialogFilename();
	std::string FileDialogFilter();

	void FileDialogReset();

};


#endif // __IMGUI_FILE_DIALOG_H_

